#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "serial_printf.h" 
#include "lcd1602.h"
#include <string.h> 


#define T1BOTTOM 65536-100
#define ser_base PD3
#define ser_braco PD5
#define ser_abraco PD6
#define ser_garra PB1

#define EEPROM_AD_SENS 4
#define EEPROM_AD_C1 7
#define EEPROM_AD_C2 10
#define EEPROM_AD_C3 13
//COLOR SNESOR PINS
#define S3 PC0
#define S2 PC1
#define S1 PC2
#define S0 PC3
#define OUT PD4

#define SW_LEFT PB0
#define SW_RIGHT PB2
#define SW_OK PB3
#define SW_PREV PB4
#define SW_NEXT PB5

#define MIN_B 8
#define MIN_Br 9+100 //falta configurar
#define MIN_ABr 8+200 //falta configurar
#define MIN_G 6+300  

//posicoes de repouso do braco
#define REP_B 20
#define REP_Br 20
#define REP_ABr 7

#define GARRA_A 14 
#define GARRA_F fechada

uint8_t servo;
uint8_t control[4];

uint8_t p_recolha[3], sensor_pos[3], cor1_box[3], cor2_box[3], cor3_box[3];

uint8_t state, next_state;
uint8_t config, next_config;

uint8_t fechada;
uint8_t le_sensor, cor1, cor2, cor3;
uint8_t red, blue, green, lido;
uint8_t move_caixa;
volatile uint8_t tempo;

volatile uint16_t T20; // em decimas de ms incrementos de 0.05ms
volatile uint16_t Tshow;
volatile uint8_t T_lcd;
volatile uint8_t T_delay;
volatile uint8_t T_delay2;

uint8_t max, min;

uint8_t cont_B, cont_Br, cont_ABr, cont_G;  //control duty cycle
uint8_t sw_left, sw_left_prev;
uint8_t sw_right, sw_right_prev;
uint8_t sw_prv, sw_prv_prev;
uint8_t sw_next, sw_next_prev;
uint8_t sw_ok, sw_ok_prev;

uint8_t pecas;

char line1[16];
char line2[16];

void tc0_init(void) {
  TCCR0B = 0x00;     // Stop TC1
  TIFR0 = 0x07;      // Clear pending intr
  TCCR0A = 0x00;     // Mode zero 
  TCNT0 = 0x00;      //Load BOTTOM   
  TIMSK0 = 0x01;     //Enable intrpt
  TCCR0B = 0x07;     //Clock externo
}



void tc1_init(void) {
  TCCR1B = 0;          // Stop TC1
  TIFR1 = (7<<TOV1)    // Clear pending intr
        | (1<<ICF1);
  TCCR1A = 0;          // Mode zero 
  TCNT1 = T1BOTTOM;    // Load BOTTOM value
  TIMSK1 = (1<<TOIE1); // Enable Ovf intrpt
  TCCR1B = 2;          // Start TC1 (TP=8) 
}



ISR(TIMER0_OVF_vect)
{
	TCNT0=0; //redefinir bottom
}

ISR(TIMER1_OVF_vect) {
  TCNT1 = T1BOTTOM;   // reload TC1
  Tshow++;
  T20++;

/*****************************/
/****GIVE PWM TO SERVOS*******/
/*****************************/
//SERVO1 -> BASE
  if(T20 >= 0  && T20 <= (MIN_B + cont_B)){
    PORTD |= (1 << ser_base);
  }
  else{
    PORTD &= ~(1 << ser_base);
  }
//SERVO2 -> BRACO
  if(T20 >= 100  && T20 <= (MIN_Br + cont_Br)){
    PORTD |= (1 << ser_braco);
  }
  else{
    PORTD &= ~(1 << ser_braco);
  }
//SERVO3 -> ANTEBRACO
  if(T20 >= 200  && T20 <= (MIN_ABr + cont_ABr)){
    PORTD |= (1 << ser_abraco);
  }
  else{
    PORTD &= ~(1 << ser_abraco);
  }
//SERVO4 -> GARRA
  if(T20 >= 300  && T20 <= (MIN_G + cont_G)){
    PORTB |= (1 << ser_garra);
  }
  else{
    PORTB &= ~(1 << ser_garra);
  }

//RELOAD PWM WAVE
  if(T20 >= 400){
    T20 = 0;
    if(T_lcd) T_lcd--;
    if(T_delay) T_delay--;
    if(T_delay2) T_delay2--;

    if(state || (config ==1)){
      if(cont_G < control[3]) cont_G++;
      else if(cont_G > control[3]) cont_G--;

      else if(cont_B < control[0]) cont_B++;
      else if(cont_B > control[0]) cont_B--;

      else if(cont_Br < control[1]) cont_Br++;
      else if(cont_Br > control[1]) cont_Br--;

      else if((cont_ABr < control[2]) && (cont_ABr < 27)) cont_ABr++;
      else if((cont_ABr > control[2]) && (cont_ABr > 5)) cont_ABr--;
    }

    else if(config >1){
      if(cont_G < control[3]) cont_G++;
      else if(cont_G > control[3]) cont_G--;

      else if(cont_Br < control[1]) cont_Br++;
      else if(cont_Br > control[1]) cont_Br--;

      else if((cont_ABr < control[2]) && (cont_ABr < 27)) cont_ABr++;
      else if((cont_ABr > control[2]) && (cont_ABr > 5)) cont_ABr--;

      else if(cont_B < control[0]) cont_B++;
      else if(cont_B > control[0]) cont_B--;

    }
    
 }

/*****************************/
/*********READ COLOURS********/
/*****************************/
  if(le_sensor && tempo == 0){
    PORTC &= ~(0x03);
    PORTC |= 0x00;
    tc0_init();
  }
  else if (le_sensor && tempo >= 60 && lido==0){
    red=TCNT0;
    TCCR0B = 0;
    PORTC |= 0x01;
    tc0_init(); 
    lido++;  
  }
  else if (le_sensor && tempo >= 120 && lido==1){
    blue=TCNT0;
    TCCR0B = 0;
    PORTC &= ~(0x01);
    PORTC |= 0x03;
    tc0_init();
    lido++;   
  }
  else if (le_sensor && tempo >= 180 && lido==2){
    green=TCNT0;
    TCCR0B = 0;
    lido++;
  }
  else if(lido==3){ //Ver isto melhor
    tempo=0;
    lido=0;

    if(red>blue && red>green){ //peça vermelha
      cor1=1;
    }
    else if(blue>green){ //peça azul
      cor3=1;
    }
    else{ //peça verde
      cor2=1;
    }
    le_sensor = 0; 
    printf("RGB: %u %u %u\n", red, green, blue);
  }

  if(le_sensor) tempo++;
}


void io_init(void)
{
  DDRD |= (1 << ser_base) | (1 << ser_braco) | (1 << ser_abraco) ; // servo1 is an output
  PORTB |= (1 << ser_base);
  DDRC |= (1 << S0) | (1 << S1) | (1 << S2) | (1 << S3); // sw input
  DDRB &= ~((1 << SW_PREV) | (1 << SW_NEXT));
  DDRB &= ~((1 << SW_LEFT) | (1 << SW_RIGHT) | (1 << SW_OK)); // sw input


  PORTD &= ~((1 << ser_base) | (1 << ser_braco) | (1 << ser_abraco)); //start low
  PORTB &= ~(1 << ser_base); //start low
  PORTC |= /*(1 <<  S1) |*/ (1 << S0); //100% frequency scalling
  PORTC &= ~(1 <<  S1);
  PORTB |= (1 << SW_LEFT) | (1 << SW_RIGHT) | (1 << SW_OK);  //pull up
  PORTB |= (1 << SW_PREV) | (1 << SW_NEXT); //pull up

  T20 = 0;
  T_lcd = 0;
  Tshow = 0; //for now
  pecas = 0;
  le_sensor = 0;


  config = 1, next_config = 1;
  state = 0, next_state = 0;

  servo = 0;
  //cont_B = REP_B, cont_Br = REP_Br, cont_ABr = REP_ABr, cont_G = REP_G; //inicial

  sw_right = 0, sw_right_prev = 0, sw_left = 0, sw_left_prev = 0;

  sprintf(line1, "VAMOS CONFIGURAR!");
  cont_B = REP_B;
  cont_Br = REP_Br;
  cont_ABr = REP_ABr;
  cont_G = GARRA_A;

  control[0] = REP_B;
  control[1] = REP_Br;
  control[2] = REP_ABr;
  control[3] = GARRA_A;

  
  p_recolha[0] = eeprom_read_byte(0x00);
  p_recolha[1] = eeprom_read_byte((uint8_t*)1);
  p_recolha[2] = eeprom_read_byte((uint8_t*)2);
  fechada = eeprom_read_byte((uint8_t*)3);

  sensor_pos[0] = eeprom_read_byte((uint8_t*)EEPROM_AD_SENS);
  sensor_pos[1] = eeprom_read_byte((uint8_t*)EEPROM_AD_SENS+1);
  sensor_pos[2] = eeprom_read_byte((uint8_t*)EEPROM_AD_SENS+2);

  cor1_box[0] = eeprom_read_byte((uint8_t*)EEPROM_AD_C1);
  cor1_box[1] = eeprom_read_byte((uint8_t*)EEPROM_AD_C1+1);
  cor1_box[2] = eeprom_read_byte((uint8_t*)EEPROM_AD_C1+2);

  cor2_box[0] = eeprom_read_byte((uint8_t*)EEPROM_AD_C2);
  cor2_box[1] = eeprom_read_byte((uint8_t*)EEPROM_AD_C2+1);
  cor2_box[2] = eeprom_read_byte((uint8_t*)EEPROM_AD_C2+2);

  cor3_box[0] = eeprom_read_byte((uint8_t*)EEPROM_AD_C3);
  cor3_box[1] = eeprom_read_byte((uint8_t*)EEPROM_AD_C3+1);
  cor3_box[2] = eeprom_read_byte((uint8_t*)EEPROM_AD_C3+2);


  //T_lcd = 255;

  le_sensor = 0;
  red=0, blue=0, green=0;
  tempo=0, lido=0;
}


int main(void)
{
  io_init();
  sei();
  tc1_init();


  printf_init();        // Init the serial port to have the ability to printf
  printf("Serial I/O Demo\n");   // into a terminal 
  lcd1602_init();
  lcd1602_clear();


  while (1) {
    sw_left_prev = sw_left;
    sw_right_prev = sw_right;
    sw_ok_prev = sw_ok;
    sw_prv_prev = sw_prv;
    sw_next_prev = sw_next;

    sw_left = !(PINB & (1 << SW_LEFT));
    sw_right = !(PINB & (1 << SW_RIGHT));
    sw_ok = !(PINB & (1 << SW_OK));
    sw_prv = !(PINB & (1 << SW_PREV));
    sw_next = !(PINB & (1 << SW_NEXT));


    if(config >= 1 && config <= 6){
      if(sw_left){
        if(control[servo] > min) control[servo]-=1;
      }
      if(sw_right){
        if(control[servo] < max) control[servo]+=1;
      }
      if(sw_prv && !sw_prv_prev){
        if(servo>0) servo--;
        lcd1602_clear();
      }
      if(sw_next && !sw_next_prev){
        if((servo<3 && config == 2) || (servo<2 && config>2)){
          servo++;
          lcd1602_clear();
        } 
      }
    }

    switch(servo){
      case 0: {
        sprintf(line2, "CONFIG. BASE"); 
        max = 39;
        min = 1;
      } break;

      case 1: {
        sprintf(line2, "CONFIG. BRACO");
        //max = 39;
        //min = 11;
      } break;

      case 2: {
        sprintf(line2, "CONFIG. A_BRACO");
        //max = 27;
        //min = 12;
      } break;

      case 3: {
        sprintf(line2, "CONFIG. GARRA");
        //max = 20;
        //min = 12;
      } break;
    }


    switch(config){
      case 1:{
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          sprintf(line1, "PONTO DE RECOLHA");
          next_config = 2;
          servo = 0;
          control[0] = p_recolha[0];
          control[1] = p_recolha[1];
          control[2] = p_recolha[2];
        }
      } break;

      case 2:{
        //print vamos configuracao do ponto de recolha
        //imprimir o servo a controlar na segunda linha
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        lcd1602_goto_xy(0,1);
        lcd1602_send_string(line2);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          next_config = 3;
          servo = 0;
          //control[3] = GARRA_A;

          for(uint8_t i=0; i<3; i++){
            p_recolha [i] = control[i]; //save posicao, FALTA GUARDAR EM MEMORIA
            control[i] = sensor_pos[i];
            eeprom_update_byte((uint8_t*)(i+0), p_recolha[i]);
          }
          fechada = control[3];
          eeprom_update_byte((uint8_t*)3, control[3]);
          sprintf(line1, "POSICAO SENSOR");
        }
      } break;

      case 3:{
        //print vamos configuracao da localizacao do sensor
        //imprimir o servo a controlar na segunda linha
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        lcd1602_goto_xy(0,1);
        lcd1602_send_string(line2);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          next_config = 4;
          servo = 0;
          for(uint8_t i=0; i<3; i++){
            sensor_pos[i] = control[i]; //save posicao, FALTA GUARDAR EM MEMORIA
            control[i] = cor1_box[i];
            eeprom_update_byte((uint8_t*)(EEPROM_AD_SENS+i), sensor_pos[i]);
          }
          sprintf(line1, "POS. CAIXA VERMELHA");
        }
      } break;

      case 4:{
        //print vamos configuracao do ponto cor1
        //imprimir o servo a controlar na segunda linha
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        lcd1602_goto_xy(0,1);
        lcd1602_send_string(line2);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          next_config = 5;
          servo = 0;
          for(uint8_t i=0; i<3; i++){
            cor1_box[i] = control[i]; //save posicao, FALTA GUARDAR EM MEMORIA
            control[i] = cor2_box[i];
            eeprom_update_byte((uint8_t*)(i+EEPROM_AD_C1), cor1_box[i]);
          }
          sprintf(line1, "POS. CAIXA VERDE");
        }
      } break;

      case 5:{
        //print vamos configuracao do ponto cor2
        //imprimir o servo a controlar na segunda linha
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        lcd1602_goto_xy(0,1);
        lcd1602_send_string(line2);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          next_config = 6;
          servo = 0;
          for(uint8_t i=0; i<3; i++){
            cor2_box[i] =  control[i]; //save posicao, FALTA GUARDAR EM MEMORIA
            control[i] = cor3_box[i];
            eeprom_update_byte((uint8_t*)(i+EEPROM_AD_C2), cor2_box[i]);
          }
          sprintf(line1, "POS. CAIXA AZUL");
        }
      } break;

      case 6:{
        //print vamos configuracao do ponto cor3
        //imprimir o servo a controlar na segunda linha
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        lcd1602_goto_xy(0,1);
        lcd1602_send_string(line2);

        if(sw_ok && !sw_ok_prev){
          lcd1602_clear();
          next_config = 7;
          servo = 0;

          for(uint8_t i=0; i<3; i++){
            cor3_box[i] =  control[i]; //save posicao, FALTA GUARDAR EM MEMORIA
            eeprom_update_byte((uint8_t*)(i+EEPROM_AD_C3), cor3_box[i]);
          }
          control[0] = REP_B;
          control[1] = REP_Br;
          control[2] = REP_ABr;
          control[3] = GARRA_A;
          //print tudo configurado!
          //print funcionamento normal a comecar 
          sprintf(line1, "TUDO PRONTO");
          sprintf(line2, "VAMOS COMECAR!");

          lcd1602_goto_xy(0,0);
          lcd1602_send_string(line1);
          lcd1602_goto_xy(0,1);
          lcd1602_send_string(line2);
          
          T_lcd = 200; //4segundos
        }
      } break;
      case 7:{
        if(!T_lcd){
          lcd1602_clear();
          next_state = 1;
          next_config = 0;
        }
      } break;
    }

    if(config != next_config){
      config = next_config;
    }

    if(sw_ok && !sw_ok_prev && (config == 0) && (state != 0)) pecas++;
    //if(sw_esq && !sw_esq_prev && (config == 0) && (state != 0)) cor1 = 1;
    //if(sw_dir && !sw_dir_prev && (config == 0) && (state != 0)) cor3 = 1;
    

    switch(state){
      case 1:{ //start está posicao repouso
        if(pecas){ //chegou tem peca_nova, manda recolher peca
          lcd1602_clear();
          sprintf(line1, "PONTO DE RECOLHA");
          next_state = 2;
          control[1] = REP_Br;
          control[2] = REP_ABr;
          control[0] = p_recolha[0];
          next_state = 2;
          T_delay = 75;
        }
        else{
          sprintf(line1, "PONTO DE RECOLHA");
          sprintf(line2, "ESPERA NOVA PECA");
          lcd1602_goto_xy(0,0);
          lcd1602_send_string(line1);
          lcd1602_goto_xy(0,1);
          lcd1602_send_string(line2);
        }
      } break;

      case 2:{
        control[1] = p_recolha[1];
        control[2] = p_recolha[2];
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);

        if(!T_delay){ //antebraco ja chegou, recolhe peca
          control[3] = GARRA_F;
          next_state = 3;
          T_delay = 50;
          pecas--; //ja tirou a peca da entrada
        }

      } break;
      case 3:{
        if(!T_delay){
          control[1] = REP_Br;
          control[2] = REP_ABr;
          T_delay = 150;
          T_delay2 = 20;
          next_state = 4;
          lcd1602_clear();
          sprintf(line1, "Sensor de Cor");
        }
      } break;

      case 4:{ 
        lcd1602_goto_xy(0,0);
        lcd1602_send_string(line1);
        if(!T_delay2){
          control[0] = sensor_pos[0];
          control[1] = sensor_pos[1];
          control[2] = sensor_pos[2];
        }

        if(!T_delay  && (cor1 == 0) && (cor2 == 0) && (cor3 == 0)){
          le_sensor = 1;
        }

        if((cor1 != 0) || (cor2 != 0) || (cor3 != 0)){ 
          control[1] = REP_Br;
          control[2] = REP_ABr;
          next_state = 5;
          T_delay = 50;
          lcd1602_clear();
          if(cor1) sprintf(line2, "Peca vermelha");
          if(cor2) sprintf(line2, "Peca verde");
          if(cor3) sprintf(line2, "Peca Azul");
        }

      } break;
      
      case 5:{
        if(!T_delay){
          lcd1602_goto_xy(0,1);
          lcd1602_send_string(line2);

          if(cor1){
          sprintf(line1, "Caixa vermelha");
          control[0] = cor1_box[0];
          move_caixa = cor1_box[1];
          control[2] = cor1_box[2];
          } 
          else if(cor2){
            sprintf(line1, "Caixa verde");
            control[0] = cor2_box[0];
            move_caixa = cor2_box[1];
            control[2] = cor2_box[2];
          } 
          else if(cor3){
            sprintf(line1, "Caixa Azul");
            control[0] = cor3_box[0];
            move_caixa = cor3_box[1];
            control[2] = cor3_box[2];
          } 
          T_delay2 = 30;
          T_delay = 75;
          lcd1602_goto_xy(0,0);
          lcd1602_send_string(line1);
          next_state = 6;
        }
      } break;
      case 6:{
        if(!T_delay2){
          control[1] = move_caixa;
        }
        if(!T_delay){
          control[3] = GARRA_A;
          control[0] = REP_B;
          control[1] = REP_Br;
          control[2] = REP_ABr;
          next_state = 1;
          cor1 = 0;
          cor2 = 0;
          cor3 = 0;
        }
      } break;
    }

      
    if(Tshow >= 400){
      printf("state / config %u / %u ....  %u-%u-%u-%u--- %u-%u-%u-%u----%u\n", state, config, cont_B, cont_Br, cont_ABr, cont_G, control[0], control[1], control[2], control[3], fechada);
      cli();
      Tshow = 0;
      sei();
    }

    if(state != next_state){
      state = next_state;
    }
  }
}
