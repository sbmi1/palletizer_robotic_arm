# Palletizer Robotic Arm

This work was carried out as part of the Microprocessor-based Systems course. The challenge consisted on assembling a robotic arm and making it capable of operating with the Palletizer function: picking up pieces, moving them, detecting their color, and dropping them off in the designated location.

For hardware, it was used an ATmega328p (Arduino Uno), 4 servo motors, TCS3200 color sensor and a 16x2 LCD. 


# Authors
João Martins up201806222@up.pt

Maria Lopes up201806775@up.pt
